package ${packageName}.mapper;

import ${packageName}.entity.${className}Entity;
import ${packageName}.util.ParamsData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ${className}Mapper {

    List<${className}Entity> getList(@Param("param") ParamsData<${className}Entity.Query,${className}Entity.Order> paramOrderPage);

    ${className}Entity getById(${primaryKeyType} id);

    int insert(${className}Entity ${className?uncap_first});

    int updateById(${className}Entity ${className?uncap_first});

    int updateByColumn(@Param("entity") ${className}Entity ${className?uncap_first},@Param("columns")List<String> columns);

    int deleteById(@Param("id") ${primaryKeyType} id);

    int deleteByParam(@Param("param") ParamsData<${className}Entity.Query,${className}Entity.Order> paramOrderPage);

}
