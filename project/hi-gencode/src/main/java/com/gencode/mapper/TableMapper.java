package com.gencode.mapper;

import com.gencode.util.MysqlStructure;

import java.util.HashMap;
import java.util.List;

public interface TableMapper {



    List<MysqlStructure> getTableInfo(String tableName);

    List<String> getTables();


}
